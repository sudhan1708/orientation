## Why docker?
    It is very hard to manage dependencies as we build large projects and if we want to deploy them into various environments.Thus, we are using docker to deploy our product.For this you don't need to master docker but need to know the basics of docker.

## Docker Terminologies

1. **Docker**
- It is just a code for developers to develop and to run applications with containers.
2. **Docker Image**
- It contains everything needed to run an applications as a container.
- code
- runtime
- libraries
- environment variables
- configuration files

3. **Container**
- It is nothing but a running Docker image.
- You can create multiple containers from one image.
4. **Docker HUb**
- It is just like GitHub but for docker images and containers.

## Docker Installation
 Procees to install docker on to your Ubuntu 16.04.
 1. Uninstall the older version of docker if is already installed

```bash
    $ sudo apt-get remove docker docker-engine docker.io containerd runc
```
2. Installing CE (Community Docker Engine)
```bash
$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

// Check if docker is successfully installed in your system
$ sudo docker run hello-world
```
- Let us have a look on the basics of docker

# docker ps
- The docker ps command allows us to view all the containers that are running on the Docker Host.

for Example:
```bash
$ docker ps

CONTAINER ID IMAGE  COMMAND CREATED        STATUS            PORTS NAMES
30986b73dc00 ubuntu "bash"  45 minutes ago Up About a minute                 elated_franklin
```
# docker start

- This command starts any stopped container(s).
- Example
```bash
$ docker start 40978
```
In the above example, Docker starts the container beginning with the container ID 40978.
```bash
$ docker start Aruna
```
In the above example, Docker starts the container beginning with the container name Aruna.

# docker run

- This command creates containers from docker images.
- Example
```bash
$ docker run Aruna
```
# docker rm

- This command deletes the containers.
- Example
```bash
$ docker rm Aruna
```

## Common Operations on Dockers
The flow of the Docker has given below.

1. Download/pull the docker images that you want to work with.
2. Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code.
- Examples:

1. **Download the docker**.
```bash
docker pull tulasi/trydock
```
2. **Run the docker image with this command**.
```bash
 docker run -ti tulasi/trydock /bin/bash
```
Every running container has an ID in your system. 

3. **Let copy our code inside docker with this command**. (Make sure you are not inside docker terminal, enter exit in command line to get out of container terminal)

- In this case, lets copy a simple python program which just prints "hello, I am talking from container". So lets consider you have this file hello.py
```bash
    print("hello, I am talking from container")
```
- Copy file inside the docker container
```bash
    docker cp hello.py e0b72ff850f8:/
```
where e0b72ff850f8 is the containerID. This will copy hello.py inside docker in root directory.

- All write a script for installing dependencies - requirements.sh
```bash
    apt update
    apt install python3
```
- Copy file inside docker
```bash
    docker cp requirements.sh e0b72ff850f8:/
```
4. **Install dependencies** If you want to install additional dependencies then you can, write all the steps for installation in a shell script and run the script inside the container.

- Give permission to run shell script
```bash
    docker exec -it e0b72ff850f8 chmod +x requirements.sh
```
- Install dependencies
```bash
    docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh
```
5. **Run the program inside container with this command**.
```bash
    docker start e0b72ff850f8
    docker exec e0b72ff850f8 python3 hello.py
```
where e0b72ff850f8 is the containerID.

6. **Save your copied program inside docker image with docker commit**.
```bash
docker commit e0b72ff850f8 snehabhapkar/trydock
```
where e0b72ff850f8 is the containerID.

7. **Push docker image to the dockerhub**.

- Tag image name with different name
```bash
    docker tag snehabhapkar/trydock username/repo
```
where username is your username on dockerhub and repo is the name of image.

For example:
```bash
docker tag snehabhapkar/trydock yh42/trial-dock-img
```

- Push on dockerhub
```bash
    docker push username/repo
```


